#ifndef UNIQUE_QPOINER_HPP
#define UNIQUE_QPOINER_HPP

#include <type_traits>

#include <QPointer>

template<typename T>
class UniqueQPointer
{
	static_assert(!std::is_pointer<T>::value, "T can't be pointer");
	static_assert(!std::is_reference<T>::value, "T can't be reference");

	T* data {nullptr};
	QPointer<T> qPointer;
public:
	constexpr UniqueQPointer() = default;

	constexpr UniqueQPointer(std::nullptr_t)
		: UniqueQPointer()
	{}

	UniqueQPointer(T* ptrData)
		: data(ptrData)
		, qPointer(ptrData)
	{}

	UniqueQPointer(UniqueQPointer&& ptr)
		: data(ptr.release())
		, qPointer(data)
	{}

	~UniqueQPointer()
	{
		static_assert(!std::is_void<T>::value, "can't delete pointer to incomplete type");
		static_assert(sizeof(T) > 0, "can't delete pointer to incomplete type");
		reset();
	}

	UniqueQPointer& operator=(UniqueQPointer&& ptr)
	{
		data = ptr.release();
		qPointer = QPointer<T>(data);
		return *this;
	}

	UniqueQPointer& operator=(std::nullptr_t)
	{
		reset();
		return *this;
	}

	T& operator*() const noexcept
	{
		return *get();
	}

	T* operator->() const noexcept
	{
		return get();
	}

	T* get() const noexcept
	{
		return data;
	}

	operator bool() const
	{
		return data != nullptr && !qPointer.isNull();
	}

	void reset(T* ptrData = nullptr)
	{
		if(data != nullptr && !qPointer.isNull())
		{
			qPointer = QPointer<T>();
			delete data;
		}
		data = ptrData;
		qPointer = QPointer<T>(ptrData);
	}

	T* release()
	{
		T* retData = data;
		qPointer = QPointer<T>();
		data = nullptr;
		return retData;
	}

	void swap(UniqueQPointer& pointer)
	{
		std::swap(data, pointer.data);
		std::swap(qPointer, pointer.qPointer);
	}

	UniqueQPointer(const UniqueQPointer&) = delete;
	UniqueQPointer& operator=(const UniqueQPointer&) = delete;
};

template<typename T>
class UniqueQPointer<T[]>
{
	static_assert(!std::is_pointer<T>::value, "T can't be pointer");
	static_assert(!std::is_reference<T>::value, "T can't be reference");
public:
	constexpr UniqueQPointer() noexcept = delete;
	constexpr UniqueQPointer(std::nullptr_t) noexcept = delete;
	UniqueQPointer(T* ptrData) noexcept = delete;
	UniqueQPointer(UniqueQPointer&& ptr) noexcept = delete;
	UniqueQPointer(const UniqueQPointer&) noexcept = delete;
	UniqueQPointer& operator=(const UniqueQPointer&) noexcept = delete;
	UniqueQPointer& operator=(UniqueQPointer&&) noexcept = delete;
	UniqueQPointer& operator=(std::nullptr_t) noexcept = delete;
	~UniqueQPointer() noexcept = delete;
	T& operator*() noexcept= delete;
	T* operator->() noexcept = delete;
	T* get() noexcept = delete;
	T* release() noexcept = delete;
	void swap(UniqueQPointer&) noexcept = delete;
	void reset() const noexcept {}
	operator bool() const noexcept {return false;}

};

template< class T, class... Args >
UniqueQPointer<T> MakeUniqueQPointer( Args&&... args )
{
	return UniqueQPointer<T>(new T(std::forward<Args>(args)...));
}

#endif
