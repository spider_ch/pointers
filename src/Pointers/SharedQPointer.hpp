#ifndef SHARED_QPOINER_HPP
#define SHARED_QPOINER_HPP

#include <exception>
#include <type_traits>
#include <atomic>

#include <QPointer>

class WeakQPointerBadPtr: public std::exception
{
public:
	virtual ~WeakQPointerBadPtr() = default;
	char const* what() const noexcept override
	{
		return "Bad QWeakPointer";
	}
};

class __SharedCounted
{
public:
	constexpr __SharedCounted() = default;
	virtual ~__SharedCounted() = default;

	void AddRefCopy()
	{
		if(useCount <= 0)
			throw WeakQPointerBadPtr();
		++useCount;
	}

	void WeakAddRef()
	{
		++weakCount;
	}

	void Release()
	{
		if(--useCount > 0)
			return;

		dispose();
		if(--weakCount == 0)
			destroy();
	}

	void WeakRelease()
	{
		if(--weakCount == 0)
			destroy();
	}

	int GetUseCount() const
	{
		return useCount;
	}

	int GetWeakCount()
	{
		return weakCount;
	}

	virtual void dispose() = 0;

	virtual void destroy()
	{
		delete this;
	}

private:
	std::atomic_int useCount {1};
	std::atomic_int weakCount {1};
};

template<typename T>
class __SharedCountedPtr final: public __SharedCounted
{
public:
	constexpr __SharedCountedPtr() = default;

	__SharedCountedPtr(T* p)
		: ptr(p)
		, qPtr(p)
	{}

	void dispose() override
	{
		if(!qPtr.isNull())
			delete ptr;
		qPtr = QPointer<T>();
		ptr = nullptr;
	}

	void destroy() override
	{
		delete this;
	}

private:
	T* ptr {nullptr};
	QPointer<T> qPtr;
};

template<typename T>
class WeakQPointer;

template<typename T>
class __WeakCount;

template<typename T>
class __SharedCount
{
public:
	constexpr __SharedCount() noexcept = default;

	__SharedCount(T* ptr)
	{
		try
		{
			counter = new __SharedCountedPtr<T>(ptr);
		}
		catch(std::exception&)
		{
			delete ptr;
			throw;
		}
	}

	__SharedCount(const __SharedCount& count)
		: counter(count.counter)
	{
		if(counter)
			counter->AddRefCopy();
	}

	__SharedCount(const __WeakCount<T>& count)
		: counter(count.counter)
	{
		if(counter)
			counter->AddRefCopy();
	}

	~__SharedCount()
	{
		if(counter)
			counter->Release();
	}

	int GetUseCount() const
	{
		return counter ? counter->GetUseCount() : 0;
	}

	bool IsUnique() const
	{
		return GetUseCount() == 1;
	}

	__SharedCount& operator=(const __SharedCount& _counter)
	{
		if(counter == _counter.counter)
			return *this;

		if(counter)
			counter->Release();

		counter = _counter.counter;
		if(counter)
			counter->AddRefCopy();

		return *this;
	}

	void swap(__SharedCount& _counter)
	{
		__SharedCountedPtr<T>* tmp = counter;
		counter = _counter.counter;
		_counter.counter = tmp;
	}

private:
	friend class __WeakCount<T>;

	__SharedCountedPtr<T>* counter {nullptr};
};

template<typename T>
class __WeakCount
{
public:
	constexpr __WeakCount() noexcept = default;

	__WeakCount(const __SharedCount<T>& count)
		: counter(count.counter)
	{
		if(counter)
			counter->WeakAddRef();
	}

	__WeakCount(const __WeakCount& count)
		: counter(count.counter)
	{
		if(counter)
			counter->WeakAddRef();
	}

	~__WeakCount()
	{
		if(counter)
			counter->WeakRelease();
	}

	int GetUseCount() const
	{
		return counter ? counter->GetUseCount() : 0;
	}

	bool IsUnique() const
	{
		return GetUseCount() == 1;
	}

	__WeakCount& operator=(const __SharedCount<T>& _counter)
	{
		if(counter == _counter.counter)
			return *this;

		if(counter)
			counter->WeakRelease();

		counter = _counter.counter;
		if(counter)
			counter->WeakAddRef();

		return *this;
	}

	__WeakCount& operator=(const __WeakCount& _counter)
	{
		if(counter == _counter.counter)
			return *this;

		if(counter)
			counter->WeakRelease();

		counter = _counter.counter;
		if(counter)
			counter->WeakAddRef();

		return *this;
	}

	__WeakCount& operator=(__WeakCount&& _counter)
	{
		if(counter)
			counter->WeakRelease();
		counter = _counter.counter;
		_counter.counter = nullptr;
		return *this;
	}

	void swap(__WeakCount& _counter)
	{
		__SharedCountedPtr<T>* tmp = counter;
		counter = _counter.counter;
		_counter.counter = tmp;
	}

private:
	friend class __SharedCount<T>;

	__SharedCountedPtr<T>* counter {nullptr};
};

template<typename T>
class SharedQPointer
{
	static_assert(!std::is_pointer<T>::value, "T can't be pointer");
	static_assert(!std::is_reference<T>::value, "T can't be reference");

public:
	constexpr SharedQPointer() = default;
	constexpr SharedQPointer(std::nullptr_t)
		: SharedQPointer()
	{}

	SharedQPointer(T* ptr)
		: data(ptr)
		, qPointer(ptr)
		, counter(ptr)
	{
		static_assert(!std::is_void<T>::value, "incomplete type");
		static_assert(sizeof(T) > 0, "incomplete type");
	}

	SharedQPointer(SharedQPointer&& ptr)
		: data(ptr.data)
		, qPointer(data)
	{
		counter.swap(ptr.counter);
		ptr.data = nullptr;
		ptr.qPointer = QPointer<T>(ptr.data);
	}

	SharedQPointer(const SharedQPointer&) = default;
	SharedQPointer& operator=(const SharedQPointer&) = default;

	SharedQPointer(const WeakQPointer<T>& weakPtr)
		: counter(weakPtr.counter)
	{
		data = weakPtr.data;
		qPointer = QPointer<T>(data);
	}

	~SharedQPointer() = default;

	SharedQPointer& operator=(SharedQPointer&& ptr)
	{
		SharedQPointer(std::move(ptr)).swap(*this);
		return *this;
	}

	void reset()
	{
		SharedQPointer().swap(*this);
	}

	void reset(T* ptr)
	{
		if(ptr == data)
			return;

		SharedQPointer(ptr).swap(*this);
	}

	typename std::add_lvalue_reference<T>::type operator*() const
	{
		return *data;
	}

	T* operator->() const
	{
		return data;
	}

	T* Get() const
	{
		return data;
	}

	operator bool() const
	{
		return data != nullptr && !qPointer.isNull();
	}

	bool unique() const
	{
		return counter.IsUnique();
	}

	int use_count() const
	{
		return counter.GetUseCount();
	}

	void swap(SharedQPointer& ptr)
	{
		std::swap(data, ptr.data);
		std::swap(qPointer, ptr.qPointer);
		counter.swap(ptr.counter);
	}

private:
	friend class WeakQPointer<T>;
	T* data {nullptr};
	QPointer<T> qPointer;
	__SharedCount<T> counter;
};

template<typename T>
class WeakQPointer
{
	static_assert(!std::is_pointer<T>::value, "T can't be pointer");
	static_assert(!std::is_reference<T>::value, "T can't be reference");

public:
	constexpr WeakQPointer() = default;
	WeakQPointer(const WeakQPointer&) = default;

	WeakQPointer(const SharedQPointer<T>& ptr)
		: data(ptr.data)
		, qPointer(data)
		, counter(ptr.counter)
	{}

	WeakQPointer(WeakQPointer&& ptr)
		: data(ptr.data)
		, qPointer(data)
		, counter(std::move(ptr.counter))
	{}

	~WeakQPointer() = default;

	WeakQPointer& operator=(const WeakQPointer&) = default;

	WeakQPointer& operator=(WeakQPointer&& ptr)
	{
		data = ptr.data;
		qPointer = ptr.qPointer;
		counter = std::move(ptr.counter);
		ptr.data = nullptr;
		ptr.qPointer = QPointer<T>();
		return *this;
	}

	WeakQPointer& operator=(const SharedQPointer<T>& ptr)
	{
		data = ptr.data;
		qPointer = ptr.qPointer;
		counter = ptr.counter;
		return *this;
	}

	SharedQPointer<T> lock() const
	{
		return SharedQPointer<T>(*this);
	}

	int use_count() const
	{
		return counter.GetUseCount();
	}

	bool expired() const
	{
		return counter.GetUseCount() == 0;
	}

	void reset()
	{
		WeakQPointer().swap(*this);
	}

	void swap(WeakQPointer& ptr)
	{
		std::swap(data, ptr.data);
		std::swap(qPointer, ptr.qPointer);
		counter.swap(ptr.counter);
	}

private:
	friend class SharedQPointer<T>;
	T* data {nullptr};
	QPointer<T> qPointer;
	__WeakCount<T> counter;
};

template<typename T>
class SharedQPointer<T[]>
{
	static_assert(!std::is_pointer<T>::value, "T can't be pointer");
	static_assert(!std::is_reference<T>::value, "T can't be reference");

public:
	constexpr SharedQPointer() = delete;
	constexpr SharedQPointer(std::nullptr_t) = delete;
	SharedQPointer(T* ptr) = delete;
	SharedQPointer(SharedQPointer&& ptr) = delete;
	SharedQPointer(const SharedQPointer&) = delete;
	SharedQPointer& operator=(const SharedQPointer&) = delete;
	SharedQPointer(const WeakQPointer<T>& weakPtr) = delete;
	~SharedQPointer() = default;
	SharedQPointer& operator=(SharedQPointer& ptr) = delete;
};

template<typename T>
class WeakQPointer<T[]>
{
	static_assert(!std::is_pointer<T>::value, "T can't be pointer");
	static_assert(!std::is_reference<T>::value, "T can't be reference");

public:
	constexpr WeakQPointer() = delete;
	WeakQPointer(const WeakQPointer&) = delete;
	WeakQPointer(const SharedQPointer<T>& ) = delete;
	WeakQPointer(WeakQPointer&&) = delete;
	~WeakQPointer() = default;
	WeakQPointer& operator=(const WeakQPointer&) = delete;
	WeakQPointer& operator=(WeakQPointer&&) = delete;
	WeakQPointer& operator=(const SharedQPointer<T>&) = delete;
};

template< class T, class... Args >
SharedQPointer<T> MakeSharedQPointer( Args&&... args )
{
	return SharedQPointer<T>(new T(std::forward<Args>(args)...));
}

#endif
