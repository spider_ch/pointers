#ifndef SHARED_QPOINER_TEST_H
#define SHARED_QPOINER_TEST_H

#include <gtest/gtest.h>

#include "../Pointers/SharedQPointer.hpp"

#include <QWidget>

TEST(shared_test, simple_test)
{
	SharedQPointer<QWidget> ptr;
	ASSERT_FALSE(ptr);

	ptr = MakeSharedQPointer<QWidget>();
	ASSERT_TRUE(ptr);
	ASSERT_EQ(ptr.use_count(), 1);
	ASSERT_TRUE(ptr.unique());

	ptr.reset();
	ASSERT_FALSE(ptr);
	ASSERT_FALSE(ptr.unique());
	ASSERT_EQ(ptr.use_count(), 0);
}

TEST(shared_test, weak_simple_test)
{
	WeakQPointer<QWidget> weakPtr;
	WeakQPointer<QWidget> weakPtr2;
	ASSERT_TRUE(weakPtr.expired());

	SharedQPointer<QWidget> ptr;
	weakPtr = ptr;
	ASSERT_TRUE(weakPtr.expired());

	ptr = MakeSharedQPointer<QWidget>();
	ASSERT_TRUE(weakPtr.expired());

	weakPtr = ptr;
	ASSERT_FALSE(weakPtr.expired());
	ASSERT_EQ(weakPtr.use_count(), 1);

	weakPtr.reset();
	ASSERT_TRUE(weakPtr.expired());

	weakPtr2 = ptr;

	weakPtr.swap(weakPtr2);
	ASSERT_TRUE(weakPtr2.expired());
	ASSERT_FALSE(weakPtr.expired());

	weakPtr2 = weakPtr;
	ASSERT_FALSE(weakPtr2.expired());
	ASSERT_FALSE(weakPtr.expired());

	ptr.reset();
	ASSERT_TRUE(weakPtr.expired());
	ASSERT_TRUE(weakPtr2.expired());
}

TEST(shared_test, parent_test)
{
	QWidget* parent = new QWidget;
	SharedQPointer<QWidget> ptr1(MakeSharedQPointer<QWidget>(parent));
	WeakQPointer<QWidget> weakPtr(ptr1);

	ASSERT_TRUE(ptr1);
	ASSERT_FALSE(weakPtr.expired());

	delete parent;

	ASSERT_FALSE(ptr1);
	ASSERT_FALSE(weakPtr.expired());
}

TEST(shared_test, move_test)
{
	SharedQPointer<QWidget> ptr1(MakeSharedQPointer<QWidget>());
	SharedQPointer<QWidget> ptr2;

	ASSERT_TRUE(ptr1);
	ASSERT_FALSE(ptr2);

	ptr2 = std::move(ptr1);

	ASSERT_FALSE(ptr1);
	ASSERT_TRUE(ptr2);

	SharedQPointer<QWidget> ptr3(std::move(ptr2));
	ASSERT_FALSE(ptr1);
	ASSERT_FALSE(ptr2);
	ASSERT_TRUE(ptr3);
}

TEST(shared_test, swap_test)
{
	SharedQPointer<QWidget> ptr1(MakeSharedQPointer<QWidget>());
	SharedQPointer<QWidget> ptr2;

	ASSERT_TRUE(ptr1);
	ASSERT_FALSE(ptr2);

	ptr1.swap(ptr2);

	ASSERT_FALSE(ptr1);
	ASSERT_TRUE(ptr2);
}

TEST(shared_test, assign_test)
{
	SharedQPointer<QWidget> ptr1(MakeSharedQPointer<QWidget>());
	SharedQPointer<QWidget> ptr2;

	ASSERT_TRUE(ptr1);
	ASSERT_FALSE(ptr2);

	ptr2 = ptr1;

	ASSERT_TRUE(ptr1);
	ASSERT_TRUE(ptr2);
	ASSERT_FALSE(ptr1.unique());
	ASSERT_EQ(ptr1.use_count(), 2);

	ptr1.reset();
	ASSERT_FALSE(ptr1);
	ASSERT_TRUE(ptr2);
	ASSERT_TRUE(ptr2.unique());
	ASSERT_EQ(ptr2.use_count(), 1);
	ASSERT_FALSE(ptr1.unique());

	ptr2 = ptr1;
	ASSERT_FALSE(ptr1);
	ASSERT_FALSE(ptr2);
}

TEST(shared_test, multi_shared_test)
{
	SharedQPointer<QWidget> ptr1(MakeSharedQPointer<QWidget>());
	SharedQPointer<QWidget> ptr2(ptr1);
	SharedQPointer<QWidget> ptr3(ptr2);

	ASSERT_TRUE(ptr1);
	ASSERT_TRUE(ptr2);
	ASSERT_TRUE(ptr3);

	ASSERT_EQ(ptr1.use_count(), 3);
	ASSERT_EQ(ptr2.use_count(), 3);
	ASSERT_EQ(ptr3.use_count(), 3);

	ptr3.reset();

	ASSERT_TRUE(ptr1);
	ASSERT_TRUE(ptr2);
	ASSERT_FALSE(ptr3);

	ASSERT_EQ(ptr1.use_count(), 2);
	ASSERT_EQ(ptr2.use_count(), 2);
	ASSERT_EQ(ptr3.use_count(), 0);
}

SharedQPointer<QWidget> GetShared()
{
	return MakeSharedQPointer<QWidget>();
}

TEST(weak_test, bad_weak_ptr)
{
	try
	{
		WeakQPointer<QWidget> badWeakPtr = GetShared();
		ASSERT_TRUE(badWeakPtr.expired());
		SharedQPointer<QWidget> sharedFromBadWeak(badWeakPtr);
		ASSERT_FALSE(true);
	}
	catch(WeakQPointerBadPtr&)
	{
		ASSERT_TRUE(true);
	}
}

TEST(weak_test, bad_weak_ptr_lock)
{
	try
	{
		WeakQPointer<QWidget> badWeakPtr = GetShared();
		ASSERT_TRUE(badWeakPtr.expired());
		badWeakPtr.lock();
		ASSERT_FALSE(true);
	}
	catch(WeakQPointerBadPtr&)
	{
		ASSERT_TRUE(true);
	}
}

TEST(weak_test, scope_test)
{
	WeakQPointer<QWidget> weakPtr;

	{
		SharedQPointer<QWidget> scopedSharedPtr = MakeSharedQPointer<QWidget>();
		weakPtr = scopedSharedPtr;

		ASSERT_FALSE(weakPtr.expired());
	}

	ASSERT_TRUE(weakPtr.expired());
}
#endif
