#ifndef UNIQUE_QPOINER_TEST_H
#define UNIQUE_QPOINER_TEST_H

#include <gtest/gtest.h>

#include "../Pointers/UniqueQPointer.hpp"

#include <QWidget>

TEST(uniqueptr_test, simple_test)
{
	UniqueQPointer<QWidget> ptr;
	ASSERT_FALSE(ptr);

	ptr = MakeUniqueQPointer<QWidget>();
	ASSERT_TRUE(ptr);

	ptr.reset();
	ASSERT_FALSE(ptr);
}

TEST(uniqueptr_test, release_test)
{
	UniqueQPointer<QWidget> ptr(MakeUniqueQPointer<QWidget>());
	ASSERT_TRUE(ptr);

	QWidget* t = ptr.release();
	ASSERT_FALSE(ptr);

	ptr.reset(t);
	ASSERT_TRUE(ptr);
}

TEST(uniqueptr_test, parent_test)
{
	QWidget* parent = new QWidget;

	UniqueQPointer<QWidget> ptr(MakeUniqueQPointer<QWidget>(parent));
	ASSERT_TRUE(ptr);

	delete parent;
	ASSERT_FALSE(ptr);
}

TEST(uniqueptr_test, move_test)
{
	UniqueQPointer<QWidget> ptr1(MakeUniqueQPointer<QWidget>());
	UniqueQPointer<QWidget> ptr2;

	ptr2 = std::move(ptr1);

	ASSERT_FALSE(ptr1);
	ASSERT_TRUE(ptr2);
}

TEST(uniqueptr_test, swap_test)
{
	UniqueQPointer<QWidget> ptr1(MakeUniqueQPointer<QWidget>());
	UniqueQPointer<QWidget> ptr2(MakeUniqueQPointer<QWidget>());

	ASSERT_TRUE(ptr1);
	ASSERT_TRUE(ptr2);

	QWidget* p1 = ptr1.get();
	QWidget* p2 = ptr2.get();

	ptr1.swap(ptr2);

	ASSERT_TRUE(ptr1);
	ASSERT_TRUE(ptr2);

	ASSERT_EQ(ptr1.get(), p2);
	ASSERT_EQ(ptr2.get(), p1);
}

#endif
