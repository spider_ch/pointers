#include "FwdTest.hpp"

#include <QWidget>

FwdTest::FwdTest()
	: m_pointer(MakeUniqueQPointer<QWidget>())
	, m_sharedPointer(MakeSharedQPointer<QWidget>())
{}

FwdTest::~FwdTest() = default;

