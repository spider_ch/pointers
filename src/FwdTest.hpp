#ifndef FWD_TEST_HPP
#define FWD_TEST_HPP

#include "Pointers/UniqueQPointer.hpp"
#include "Pointers/SharedQPointer.hpp"

class QWidget;

class FwdTest
{
public:
	FwdTest();
	~FwdTest();

private:
	UniqueQPointer<QWidget> m_pointer;
	SharedQPointer<QWidget> m_sharedPointer;
};

#endif
