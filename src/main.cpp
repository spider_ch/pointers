#include <gtest/gtest.h>

#include "FwdTest.hpp"

#include <QApplication>

#include "Pointers/UniqueQPointer.hpp"
#include "Pointers/SharedQPointer.hpp"

#include "Tests/UniqueQPointerTest.h"
#include "Tests/SharedQPointerTest.h"

int main(int argc, char** argv)
{
	QApplication app(argc, argv);

	FwdTest t;
	(void)t;

	::testing::InitGoogleTest(&argc, argv);
	return RUN_ALL_TESTS();
}
